import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';

const INGREDIENT_PRICES = {
    salad: 0.5,
    meat: 1.4,
    cheese: 0.4,
    bacon: 0.7
}

export const addIngredient = (name) => {
    return {
        type: actionTypes.ADD_INGREDIENT,
        ingredientName: name
    };
};

export const removeIngredient = (name) => {
    return {
        type: actionTypes.REMOVE_INGREDIENT,
        ingredientName: name
    };
};


export const setProperties = (properties) => {
    return {
        type: actionTypes.SET_PROPERTIES,
        properties: properties
    }
}

export const fetchIngredientsFailed = () => {
    return {
        type: actionTypes.FETCH_INGREDIENTS_FAILED
    }
}

export const initIngredients = () => {
    return dispatch => {
        axios.get('https://reactive-burger-2d4dd.firebaseio.com/ingredients.json')
            .then(response => {
                let ingredients = {};
                let price = 0;
                Object.keys(response.data).map(key => {
                    ingredients[key] = 0;
                    return [...Array((response.data)[key])].map(elem => {
                        ingredients[key]++;
                        price += INGREDIENT_PRICES[key];
                        return elem;
                    })
                });
                dispatch(setProperties({ingredients: ingredients, purchasable: price > 0, totalPrice: price}));
            })
            .catch(error => {
                console.log(error);
                dispatch(fetchIngredientsFailed());
            });
    }
}