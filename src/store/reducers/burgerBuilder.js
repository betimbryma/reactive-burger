import * as actionTypes from '../actions/actionTypes';

const initialState = {
    ingredients: null,
    totalPrice: 4,
    error: false,
    building: false
};

const INGREDIENT_PRICES = {
    salad: 0.5,
    meat: 1.4,
    cheese: 0.4,
    bacon: 0.7
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_INGREDIENT:
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingredientName]: state.ingredients[action.ingredientName] + 1
                },
                totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName],
                purchasable: true,
                building: true
            }
        case actionTypes.REMOVE_INGREDIENT:
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingredientName]: state.ingredients[action.ingredientName] - 1
                },
                totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName],
                building: true,
                purchasable: (state.totalPrice - INGREDIENT_PRICES[action.ingredientName]) > initialState.totalPrice
            }
        case actionTypes.SET_PROPERTIES:
            return {
                ...state,
                ingredients: action.properties.ingredients,
                purchasable: action.properties.purchasable,
                building: false,
                totalPrice: initialState.totalPrice + action.properties.totalPrice,
                error: false
            }
        case actionTypes.FETCH_INGREDIENTS_FAILED: 
            return {
                ...state,
                error: true
            }
        default:
            return state;
    }
    
};

export default reducer;