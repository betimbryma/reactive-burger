import axios from 'axios';

const instance = axios.create(
    {
        baseURL: 'https://reactive-burger-2d4dd.firebaseio.com/'
    }
);

export default instance;