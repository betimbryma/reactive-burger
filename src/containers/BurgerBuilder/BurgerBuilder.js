import React, { Component } from 'react';
import { connect } from 'react-redux';

import Aux from '../../hoc/Aux/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import axios from '../../axios-orders';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErorHandler';
import * as burgerBuilderActions from '../../store/actions/';

export class BurgerBuilder extends Component{

    state = {
        purchasing: false
    }

    componentDidMount() {
        this.props.onInitIngredients();
    }

    updatePurchaseState(ingredients) {

        const sum = Object.keys(ingredients).map(elem => {
            return ingredients[elem];
        }).reduce((num, elem) => {
            return num+elem;
        }, 0);

        return sum > 0;
    }
    
    purchaseHandler = () => {
        if(this.props.isAuthenticated)
            this.setState({purchasing: true});
        else {
            this.props.onSetAuthRedirectPath('/checkout');
            this.props.history.push('/auth');
        }
    }

    purchaseCancelHandler = () => {
        this.setState({purchasing: false});
    }

    purchaseContinueHandler = () => {
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
    }

    render() {
        const disableInfo = {
            ...this.props.ingredients
        };
        for(let key in disableInfo){
            disableInfo[key] = disableInfo[key] <= 0;
        }

        let orderSummary = 
            this.props.ingredients ? <OrderSummary ingredients = {this.props.ingredients}
                purchaseCancelled={this.purchaseCancelHandler}
                purchaseContinued={this.purchaseContinueHandler}
                price={this.props.price}/> : null;

        let burger = !this.props.ingredients ? 
            this.props.error ? <p style ={{textAlign: 'center'}}>Ingredients cannot be loaded :(</p> : <Spinner /> : 
            <Aux>
                <Burger ingredients = {this.props.ingredients}></Burger>
                <BuildControls
                    ingredientAdded={this.props.onIngredientAdded}
                    ingredientRemoved={this.props.onIngredientRemoved}
                    disabled={disableInfo}
                    totalPrice={this.props.price}
                    purchasable={this.props.purchasable}
                    isAuth={this.props.isAuthenticated}
                    ordered={this.purchaseHandler}/>
            </Aux>
        ;
        
        return(
            <Aux>
                <Modal display={this.state.purchasing}
                    modalClosed={this.purchaseCancelHandler}> {orderSummary} </Modal>
                {burger}
            </Aux>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded: (name) => dispatch(burgerBuilderActions.addIngredient(name)),
        onIngredientRemoved: (name) => dispatch(burgerBuilderActions.removeIngredient(name)),
        onInitIngredients: () => dispatch(burgerBuilderActions.initIngredients()),
        onInitPurchase:() => dispatch(burgerBuilderActions.purchaseInit()),
        onSetAuthRedirectPath : (path) => dispatch(burgerBuilderActions.setAuthRedirectPath(path))
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        purchasable: state.burgerBuilder.purchasable,
        isAuthenticated: state.auth.token !== null
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));
